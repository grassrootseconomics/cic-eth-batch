# standard imports
import os
import logging
import argparse
import uuid
import sys
import stat
import socket

# external imports
import confini
from xdg.BaseDirectory import get_runtime_dir

logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

config_dir = os.environ.get('CONFINI_DIR', '.')
default_queue_runtime_path = os.path.join(get_runtime_dir(), 'chainqueue')

argparser = argparse.ArgumentParser('chainqueue transaction submission and trigger server')
argparser.add_argument('-c', '--config', dest='c', type=str, default=config_dir, help='configuration directory')
argparser.add_argument('--session-id', dest='session_id', type=str, default=str(uuid.uuid4()), help='session id to use for session')
argparser.add_argument('-s', '--socket-path', dest='s', type=str, help='socket path')
argparser.add_argument('--env-prefix', default=os.environ.get('CONFINI_ENV_PREFIX'), dest='env_prefix', type=str, help='environment prefix for variables to overwrite configuration')
argparser.add_argument('-v', action='store_true', help='be verbose')
argparser.add_argument('-vv', action='store_true', help='be very verbose')
argparser.add_argument('input_dir', type=str, help='directory with transaction files')
args = argparser.parse_args(sys.argv[1:])

if args.vv:
    logg.setLevel(logging.DEBUG)
elif args.v:
    logg.setLevel(logging.INFO)

config = confini.Config(args.c)
socket_path = getattr(args, 's')
args_override = {
        'QUEUE_SOCKET_PATH': getattr(args, 's'),
        }
config.dict_override(args_override, 'cli args')
config.process()
config.add(getattr(args, 'input_dir'), '_INPUT_DIR', True)
config.add(getattr(args, 'session_id'), '_SESSION_ID', True)
if not config.get('QUEUE_SOCKET_PATH'):
    config.add(os.path.join(default_queue_runtime_path, config.get('_SESSION_ID'), 'chainqueue.sock'), 'QUEUE_SOCKET_PATH', True)
logg.debug('config loaded:\n{}'.format(config))


def main():
    for p in os.listdir(config.get('_INPUT_DIR')):
        cli = socket.socket(family=socket.AF_UNIX, type=socket.SOCK_STREAM)
        cli.connect(config.get('QUEUE_SOCKET_PATH'))
        if p[0] == '.':
            continue
        fpath = os.path.join(config.get('_INPUT_DIR'), p)
        f = open(fpath, 'r')
        tx = f.read()
        f.close()
        r = cli.send(tx.encode('utf-8'))
        logg.debug('sent {} bytes'.format(r))
        r = cli.recv(4)
        cli.close()
        if int.from_bytes(r, byteorder='big'):
            sys.stderr.write('error {}'.format(r))
            sys.exit(1)
        os.unlink(fpath)
        logg.debug('submitted {} {} status {}'.format(fpath, tx, r))

if __name__ == '__main__':
    main()
